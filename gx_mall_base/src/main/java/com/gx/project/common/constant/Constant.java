package com.gx.project.common.constant;

/**
 * 系统常量
 * @author:gx
 * @version:1.0
 * @since:1.0
 * @createTime:2023-03-17 17:05:15
 */
public interface Constant {

    /**
     * 系统APPid（接口常量默认都是 public satic final 的）
     */
    String APPID = "";

}
