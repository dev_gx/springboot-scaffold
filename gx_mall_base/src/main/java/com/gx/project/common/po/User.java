package com.gx.project.common.po;

import lombok.Data;

/**
 * @author gx
 * @description
 */
public class User implements java.io.Serializable{

    /** 用户名*/
    private String name;

    private Long name_id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getName_id() {
        return name_id;
    }

    public void setName_id(Long name_id) {
        this.name_id = name_id;
    }
}
