package com.gx.project.common.util;

/**
 * 字符串工具类
 * @author:gx
 * @version:1.0
 * @since:1.0
 * @createTime:2023-03-17 17:05:15
 */
public final class  StringUtil{

    private StringUtil(){

    }


    /**
     * 判断字符串是否为空
     * @param str 待验证的字符串
     * @return true 为空  false 不为空
     */
    public static boolean isNotEmpty( String str ){
        return (str != null && !str.isEmpty());
    }


}
