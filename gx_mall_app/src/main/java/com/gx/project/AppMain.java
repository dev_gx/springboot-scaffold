package com.gx.project;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * app入口
 * @author:gx
 * @version:1.0
 * @since:1.0
 * @createTime:2023-03-17 17:05:15
 */
@SpringBootApplication
@MapperScan({"com.gx.project.*.dao"})
public class AppMain {

    public static void main(String[] args)
    {
        SpringApplication.run(AppMain.class, args);
    }
}
