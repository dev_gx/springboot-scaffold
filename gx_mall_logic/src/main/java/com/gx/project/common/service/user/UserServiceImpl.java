package com.gx.project.common.service.user;

import com.gx.common.result.ResponseResult;
import com.gx.project.common.dao.BaseDao;
import com.gx.project.common.domain.user.UserDomain;
import com.gx.project.common.po.User;
import com.gx.project.common.service.AbstractBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ImportResource;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author gx
 * @description
 */
@Service("userService")
public class UserServiceImpl extends AbstractBaseService<User,Long> implements UserService{

    @Resource
    private UserDomain userDomain;

    @Override
    protected BaseDao<User, Long> getBaseDao() {
        return null;
    }

    @Override
    public Boolean saveOrUpdate(User user) {
        return null;
    }

    @Override
    public ResponseResult addUser(String name) {
        userDomain.addUser(name);
        return ResponseResult.successInstance();
    }
}
