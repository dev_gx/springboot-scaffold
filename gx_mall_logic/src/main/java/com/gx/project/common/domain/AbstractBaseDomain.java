package com.gx.project.common.domain;

import com.gx.project.common.dao.BaseDao;

import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.HashMap;
import java.util.List;


/**
 * base domain abstract
 * @author:gx
 * @version:1.0
 * @since:1.0
 * @createTime:2023-03-17 17:05:15
 */
public abstract class AbstractBaseDomain<T, C> implements BaseDomain<T, C> {

    @Override
    public T getById(C id) {
        return getBaseDao().selectById(id);
    }

    @Override
    public Map<String,Object> getListByPage(Map<String, Object> params, Integer pageNo, Integer pageSize) {
        int startIndex = 0;
        int fetchSize = 0;
        if ( pageNo != null && pageSize != null ) {
            startIndex = (pageNo-1) * pageSize;
            fetchSize = pageSize;
        }
		Map<String,Object> map = new HashMap<String,Object>(2);	
        int i = getBaseDao().selectCountByParam(params);
		map.put("total", i);
        if( i > 0 ) {
            map.put("data", getBaseDao().selectByParam(params,startIndex,fetchSize));
        }
        return map;
    }

    @Transactional
    @Override
    public Boolean removeById(C id) {
        return getBaseDao().deleteById(id) > 0;
    }

    @Transactional
    @Override
    public Boolean save(T t) {
        return getBaseDao().insert(t) > 0;
    }

    @Transactional
    @Override
    public Boolean update(T t) {
        return getBaseDao().update(t) > 0;
    }

    protected abstract BaseDao<T,C> getBaseDao();
}
