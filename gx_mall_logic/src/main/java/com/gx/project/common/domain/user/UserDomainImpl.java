package com.gx.project.common.domain.user;

import com.gx.project.common.dao.BaseDao;
import com.gx.project.common.dao.user.UserDao;
import com.gx.project.common.domain.AbstractBaseDomain;
import com.gx.project.common.po.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author gx
 * @description
 */
@Service("userDomain")
public class UserDomainImpl extends AbstractBaseDomain<User,Long> implements UserDomain{

    @Resource
    private UserDao userDao;


    @Override
    protected BaseDao<User, Long> getBaseDao() {
        return null;
    }

    @Override
    public List<User> getAll(Map<String, Object> params) {
        return null;
    }
    @Transactional
    public void addUser(String name) {
        User dto = new User();
        dto.setName(name);
        userDao.insert(dto);
    }
}
