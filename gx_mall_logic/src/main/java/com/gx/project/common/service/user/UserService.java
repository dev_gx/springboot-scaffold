package com.gx.project.common.service.user;

import com.gx.common.result.ResponseResult;
import com.gx.project.common.dao.BaseDao;
import com.gx.project.common.po.User;
import com.gx.project.common.service.BaseService;

/**
 * @author gx
 * @description
 */
public interface UserService extends BaseService<User,Long> {


    ResponseResult addUser(String name);
}
