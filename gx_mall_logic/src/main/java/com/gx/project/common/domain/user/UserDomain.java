package com.gx.project.common.domain.user;

import com.gx.project.common.domain.BaseDomain;
import com.gx.project.common.po.User;

/**
 * @author gx
 * @description
 */
public interface UserDomain extends BaseDomain<User,Long> {

    void addUser(String name);

}
