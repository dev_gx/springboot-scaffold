package com.gx.project.common.dao.user;

import com.gx.project.common.dao.BaseDao;
import com.gx.project.common.po.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author gx
 * @description
 */
@Mapper
public interface UserDao extends BaseDao<User,Long> {



    User addUser(@Param("name") String name);



}
