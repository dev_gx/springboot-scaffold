package com.gx.project.common.domain;

import java.util.Map;
import java.util.List;


/**
 * base domain
 * @author:gx
 * @version:1.0
 * @since:1.0
 * @createTime:2023-03-17 17:05:15
 */
public interface BaseDomain<T,C> {

    /**
     * 根据id获得对象
     * @param id
     * @return
     */
    T getById(C id);

    /**
     * 分页查询对象列表
     * @param params
     * @param pageNo
     * @param pageSize
     * @return {"data": List<T>, "total": 123}
     */
    Map<String,Object> getListByPage(Map<String, Object> params, Integer pageNo, Integer pageSize);

    /**
     * 通过id删除
     * @param id
     * @return
     */
    Boolean removeById(C id);
	
	/**
     * 保存
     * @param t
     * @return
     */	
	Boolean save(T t);

    /**
     * 更新
     * @param t
     * @return
     */
    Boolean update(T t);

    /**
     * 查询所有
     * @param params 参数，非必填
     * @return
     */
    List<T> getAll(Map<String,Object> params);

}
