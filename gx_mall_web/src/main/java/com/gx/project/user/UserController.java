package com.gx.project.user;

import com.gx.common.result.ResponseResult;
import com.gx.project.common.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author gx
 * @description
 */
@RestController
@RequestMapping("/user")
@Validated
public class UserController {


    @Autowired
    private UserService userService;


    @RequestMapping("/addUser")
    public ResponseResult applyToSupplier(@NotNull(message = "name必填") String name){
        try{
            return userService.addUser(name);
        }
        catch (Exception ex){
            return ResponseResult.failInstance(ex.getMessage());
        }
    }



}
