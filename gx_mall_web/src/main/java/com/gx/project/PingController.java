package com.gx.project;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author gx
 * @description
 */
@RestController
@RequestMapping("/root")
public class PingController {


    @RequestMapping("/ping")
    public Integer ping(){
        return 200;
    }

}
